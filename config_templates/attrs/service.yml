---
- name: "urn:perun:facility:attribute-def:def:serviceName"
  display_name:
    en: "Name"
    cs: "Jméno"
  description:
    en: "Name of the service"
    cs: "Jméno služby"
  required_test: True
  required_prod: True
  displayed: True
  editable: True
  type: "java.util.LinkedHashMap"
  display_position: 1
  allowed_keys: ["en", "cs"]
- name: "urn:perun:facility:attribute-def:def:serviceDescription"
  display_name:
    en: "Description"
    cs: "Popis"
  description:
    en: "Short description of the service in plain language understandable for common end users (max 255 characters)."
    cs: "Krátký popis služby pro koncové uživatele (max 255 znaků)"
  required_test: True
  required_prod: True
  displayed: True
  editable: True
  type: "java.util.LinkedHashMap"
  display_position: 2
  allowed_keys: ["en", "cs"]
- name: "urn:perun:facility:attribute-def:def:spInformationURL"
  display_name:
    en: "Information URL"
    cs: "Informace o službě"
  description:
    en: "Link where user can find information about the service. If such site does not exist, please put link to the organization website."
    cs: "Odkaz na informace o službě. Může také obsahovat informace o organizaci provozující službu."
  required_test: False
  required_prod: True
  displayed: True
  editable: True
  type: "java.util.LinkedHashMap"
  display_position: 3
  regex: "https://.*"
  allowed_keys: ["en", "cs"]
- name: "urn:perun:facility:attribute-def:def:rpLoginURL"
  display_name:
    en: "Login URL"
    cs: "URL přihlašovací stránky"
  description:
    en: "Link where users can access the service"
    cs: "Odkaz pro přístup ke službě"
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.lang.String"
  display_position: 4
  regex: "https://.*"
- name: "urn:perun:facility:attribute-def:def:RaS"
  display_name:
    en: "Research and Scholarship"
    cs: "Služba pochází z oblasti výzkumu a vzdělávaní"
  description:
    en: 'Service adheres to <a href="https://refeds.org/category/research-and-scholarship" target="_blank">Research and Scholarship Entity Category</a>'
    cs: 'Zaškrtněte, pokud služba splňuje podmínky kategorie <a href="https://refeds.org/category/research-and-scholarship" target="_blank">Research and Scholarship Entity Category</a>'
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.lang.Boolean"
  display_position: 5
- name: "urn:perun:facility:attribute-def:def:spPrivacyPolicyURL"
  display_name:
    en: "Privacy policy URL"
    cs: "Dokument o ochraně osobních údajů"
  description:
    en: "Link to the privacy policy document of the organization or service (required for external services)"
    cs: "Odkaz na dokument o ochraně osobních údajů (povinné pro externí služby)"
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.util.LinkedHashMap"
  display_position: 6
  regex: "https://.*"
  allowed_keys: ["en", "cs"]
- name: "urn:perun:facility:attribute-def:def:administratorContact"
  display_name:
    en: "Registrator contact"
    cs: "Kontakt na registrátora služby"
  description:
    en: "Email of the persons, who register the service"
    cs: "Email osoby, která registrovala službu"
  required_test: True
  required_prod: True
  displayed: True
  editable: True
  type: "java.lang.String"
  display_position: 7
  regex: "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*"
- name: "urn:perun:facility:attribute-def:def:spAdminContact"
  display_name:
    en: "Administrative contacts"
    cs: "Administrativní kontakty"
  description:
    en: "Email of the persons responsible for the service (generic addresses if possible)"
    cs: "Emaily na osoby zodpovědné za provoz služby (pokud možno generické adresy)"
  required_test: True
  required_prod: True
  displayed: True
  editable: True
  type: "java.util.ArrayList"
  display_position: 8
  regex: "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*"
- name: "urn:perun:facility:attribute-def:def:spSupportContact"
  display_name:
    en: "Support contacts"
    cs: "Kontakt na uživatelskou podporu"
  description:
    en: "User support email"
    cs: "Emaily uživatelské podpory"
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.util.ArrayList"
  display_position: 9
  regex: "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*"
- name: "urn:perun:facility:attribute-def:def:spSecurityContact"
  display_name:
    en: "Security contacts"
    cs: "Kontakt pro hlášení bezpečnostních incidentů"
  description:
    en: "Security contacts"
    cs: "Kontakt pro hlášení bezpečnostních incidentů"
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.util.ArrayList"
  display_position: 10
  regex: "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*"
- name: "urn:perun:facility:attribute-def:def:spInternal"
  display_name:
    en: "This service is internal to MU"
    cs: "Žádám o zařazení mezi interní služby MU"
  description:
    en: "The service is operated by MU on MU servers, it is primarily study or work related and no third parties have access to personal data"
    cs: "Službu provozuje MU na serverech MU, slouží primárně ke studiu nebo zaměstnání a k osobním údajům nemají přístup žádné třetí strany"
  required_test: False
  required_prod: False
  displayed: True
  editable: True
  type: "java.lang.Boolean"
  display_position: 11
  # SP category
- name: "urn:perun:facility:attribute-def:def:rpCategory"
  display_name:
    en: "Category of the service"
    cs: "Kategorie služby"
  description:
    en: "Choose the most appropriate category for your service. If none apply, choose other."
    cs: "Zvolte co nejpřesnější kategorii pro svou službu. Pokud žádná nevyhovuje, zvolte hodnotu other."
  allowed_values:
    - electronic information resources
    - libraries
    - maps
    - security
    - IT supporting services
    - network
    - information systems
    - accounts and identities
    - user support
    - collaboration
    - audio video
    - education
    - software and hardware
    - web and PR
    - data
    - cloud
    - other
  required_test: True
  displayed: True
  editable: True
  type: "java.lang.String"
