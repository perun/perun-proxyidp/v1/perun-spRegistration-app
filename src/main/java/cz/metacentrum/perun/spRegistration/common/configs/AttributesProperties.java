package cz.metacentrum.perun.spRegistration.common.configs;

import java.util.*;
import jakarta.annotation.PostConstruct;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Getter
@Setter
@EqualsAndHashCode
@Slf4j
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "attributes")
public class AttributesProperties {

    @NonNull private Names names;
    @NonNull private Values values;

    @PostConstruct
    public void postInit() {
        log.info("Initialized attributes properties");
        log.debug("{}", this);
    }

    public List<String> getAttrNames() {
        return names.getAttrNames();
    }

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Names {
        // attr names
        @NotBlank
        private String userEmail = "urn:perun:user:attribute-def:def:preferredMail";

        @NotBlank
        private String proxyIdentifier = "urn:perun:facility:attribute-def:def:proxyIdentifiers";

        @NotBlank
        private String masterProxyIdentifier = "urn:perun:facility:attribute-def:def:masterProxyIdentifier";

        @NotBlank
        private String isTestSp = "urn:perun:facility:attribute-def:def:isTestSp";

        @NotBlank
        private String showOnServiceList = "urn:perun:facility:attribute-def:def:showOnServiceList";

        @NotBlank
        private String administratorContact = "urn:perun:facility:attribute-def:def:administrationContact";

        @NotBlank
        private String oidcClientId = "urn:perun:facility:attribute-def:def:OIDCClientID";

        @NotBlank
        private String oidcClientSecret = "urn:perun:facility:attribute-def:def:OIDCClientSecret";

        @NotBlank
        private String entityId = "urn:perun:facility:attribute-def:def:entityID";

        @NotBlank
        private String isOidc = "urn:perun:facility:attribute-def:def:isOidcFacility";

        @NotBlank
        private String isSaml = "urn:perun:facility:attribute-def:def:isSamlFacility";

        @NotBlank
        private String serviceName = "urn:perun:facility:attribute-def:def:serviceName";

        @NotBlank
        private String serviceDesc = "urn:perun:facility:attribute-def:def:serviceDescription";

        @NotBlank
        private String managerGroup = "urn:perun:facility:attribute-def:def:adminGroup";

        @NotBlank
        private String updatedAt = "urn:perun:facility:attribute-def:def:rpUpdatedAt";

        private String serviceCategory;

        private String mfaCategories;

        private final List<String> attrNames = new ArrayList<>();

        private List<String> getAttrNames() {
            if (attrNames.size() == 0) {
                addMandatoryAttrName(userEmail);
                addMandatoryAttrName(proxyIdentifier);
                addMandatoryAttrName(masterProxyIdentifier);
                addMandatoryAttrName(isTestSp);
                addMandatoryAttrName(showOnServiceList);
                addMandatoryAttrName(administratorContact);
                addMandatoryAttrName(oidcClientId);
                addMandatoryAttrName(oidcClientSecret);
                addMandatoryAttrName(entityId);
                addMandatoryAttrName(isOidc);
                addMandatoryAttrName(isSaml);
                addMandatoryAttrName(serviceName);
                addMandatoryAttrName(serviceDesc);
                addMandatoryAttrName(managerGroup);
                addMandatoryAttrName(updatedAt);
                addOptionalAttrName(mfaCategories);
                addOptionalAttrName(serviceCategory);
            }
            return attrNames;
        }

        private void addMandatoryAttrName(String attrName) {
            if (!StringUtils.hasText(attrName)) {
                throw new IllegalStateException("Missing mandatory attr name");
            }
            attrNames.add(attrName);
        }

        private void addOptionalAttrName(String attrName) {
            if (StringUtils.hasText(attrName)) {
                attrNames.add(attrName);
            }
        }
    }

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Values {
        @NotBlank
        private String proxyIdentifier = "https://login.cesnet.cz/idp/";

        @NotNull
        private List<String> additionalProxyIdentifiers = new ArrayList<>();

        @NotBlank
        private String masterProxyIdentifier = "https://login.cesnet.cz/idp/";

        private final List<RpCategory> rpCategories = new ArrayList<>();
    }

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RpCategory {

        @NotBlank
        private String category;

        @NotEmpty
        @NotNull
        private final Map<String, String> labels = new HashMap<>();

    }

}
