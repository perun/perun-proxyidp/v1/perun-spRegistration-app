package cz.metacentrum.perun.spRegistration.web.models;

import cz.metacentrum.perun.spRegistration.common.models.PerunAttribute;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RegisterServiceBody {

    private String protocol;

    private List<PerunAttribute> attributes;

}
