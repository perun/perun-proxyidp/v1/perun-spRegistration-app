package cz.metacentrum.perun.spRegistration.web;

import static cz.metacentrum.perun.spRegistration.web.controllers.AuthController.SESS_ATTR_USER;

import cz.metacentrum.perun.spRegistration.common.configs.AppBeansContainer;
import cz.metacentrum.perun.spRegistration.common.models.User;
import cz.metacentrum.perun.spRegistration.persistence.adapters.PerunAdapter;
import cz.metacentrum.perun.spRegistration.persistence.exceptions.PerunConnectionException;
import cz.metacentrum.perun.spRegistration.persistence.exceptions.PerunUnknownException;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@Slf4j
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final PerunAdapter adapter;
    private final AppBeansContainer appBeansContainer;

    public AuthSuccessHandler(PerunAdapter adapter, AppBeansContainer appBeansContainer) {
        super();
        this.adapter = adapter;
        this.appBeansContainer = appBeansContainer;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication)
        throws ServletException, IOException
    {
        trySetUser(
            request,
            authentication,
            appBeansContainer.getApplicationProperties().getProxyIdentifier(),
            appBeansContainer.getAttributesProperties().getNames().getUserEmail(),
            appBeansContainer.getPerunAdapter()
        );
        super.onAuthenticationSuccess(request, response, authentication);
    }

    public static void trySetUser(HttpServletRequest request,
                                  Authentication authentication,
                                  String proxyIdentifier,
                                  String userEmailAttr,
                                  PerunAdapter perunAdapter) {
        if (authentication != null && authentication.getAuthorities() != null) {
            log.debug("User '{}' has logged in", authentication.getName());
            String sub = authentication.getName();
            boolean isAdmin = authentication.getAuthorities() != null
                && authentication.getAuthorities().contains(SecurityConfiguration.ROLE_ADMIN);
            try {
                User u = perunAdapter.getUserWithEmail(sub, proxyIdentifier, userEmailAttr);
                if (u != null) {
                    u.setAppAdmin(isAdmin);
                }
                HttpSession sess = request.getSession();
                if (sess == null) {
                    log.error("No session available, cannot store Perun USER object");
                } else {
                    sess.setAttribute(SESS_ATTR_USER, u);
                }
            } catch (PerunUnknownException | PerunConnectionException e) {
                log.warn("Could not fetch user from Perun");
                log.debug("Details: ", e);
            }
        }
    }

}
