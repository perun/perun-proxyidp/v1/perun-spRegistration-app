# Perun application for SP registration

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2024)

This project has reached end of life, which means no new features will be added. Security patches and important bug fixes will end as of 2024. Check out [Federation registry](https://github.com/rciam/rciam-federation-registry) instead.

## Description

See [Development.md](./Development.md) and [Production.md](./Production.md) for more information.

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).
Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

### Available scopes

- api
- gui
