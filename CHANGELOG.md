## [7.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/compare/v7.0.1...v7.0.2) (2025-03-05)


### Bug Fixes

* removed problematic autowired annotations ([d9a3462](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/commit/d9a3462696cef05379b1570d6e75cab3b6613d78))

## [7.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/compare/v7.0.0...v7.0.1) (2025-02-28)


### Bug Fixes

* update spring boot and other dependencies ([bbcb6de](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/commit/bbcb6dec014dd98c0aa8c8bdadc6652e29c4b184))

# [7.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/compare/v6.0.0...v7.0.0) (2025-02-28)


### Bug Fixes

* bw logo ([ffb2e5c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/commit/ffb2e5cc62aec12e5c13ce3c4b2f0f1bb5c7e46e))
* make csrf work again ([5601342](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/commit/5601342c7fc40dadc7f04cf883dcfaf04805e896))


### Features

* update to Spring Boot 3 ([7277542](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/perun-spRegistration-app/commit/7277542e973d52a13ada915c11fd6d0e2c5f72b4))


### BREAKING CHANGES

* Requires Java 17 and Tomcat 10

# [6.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.2.2...v6.0.0) (2024-01-16)


### Bug Fixes

* 🐛 Registering new service ([0ab1bea](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/0ab1beab32fde68b91f7f684be3fa80e59eb24e1))


### BREAKING CHANGES

* 🧨 Requries DB update

## [5.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.2.1...v5.2.2) (2024-01-16)


### Bug Fixes

* 🐛 Passing entityID to isOidcRequest instead of bool attr ([dfb3072](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/dfb30724bb65a3d01ac6af41f456ec8b71ce1775))

## [5.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.2.0...v5.2.1) (2024-01-11)


### Bug Fixes

* 🐛 Fix proper attributes loading based on isSaml/isOidc ([31d95b6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/31d95b6acf171dfb4678de379bd82d2bcc88c57c))

# [5.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.5...v5.2.0) (2023-09-08)


### Features

* 🎸 MFA Categories configurable entityless attr key ([bb247ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/bb247ad225fe8ccc4bb21c61f39bfed77fe91fee))

## [5.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.4...v5.1.5) (2023-08-04)


### Bug Fixes

* commit maven-wrapper.jar ([932803f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/932803f366b6cc6a6577e09204af50d17d7a6544))
* upgrade maven wrapper to official 3.2.0 ([071c947](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/071c9475663ce5e4740afd2706c710c8e1be12f1))

## [5.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.3...v5.1.4) (2023-08-04)


### Bug Fixes

* revert maven-wrapper to 0.5.6 ([74d9a60](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/74d9a6082fe8bee60f72776c8bc6cdf1bbcf1a47))

## [5.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.2...v5.1.3) (2023-08-03)


### Bug Fixes

* 🐛 Fix updating MFA categories ([a3ab8a2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/a3ab8a2b5c9c961ffd57d98a17d7f6224d3fb174))

## [5.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.1...v5.1.2) (2023-07-26)


### Bug Fixes

* 🐛 Fix loading MFA categories attributes ([75fab0c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/75fab0c1358b800d21806d366570f120d0d8ac87))

## [5.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.1.0...v5.1.1) (2023-07-26)


### Bug Fixes

* 🐛 Fix sanitizing inputs ([c792e5f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/c792e5f4804266b5ce70f52e2d514bda64f2ae1c))

# [5.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.14...v5.1.0) (2023-07-18)


### Features

* update mfaCategories on service change ([9e1e3e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/9e1e3e7b87939ee04b29db7f3e5ce25562b21936))

## [5.0.14](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.13...v5.0.14) (2023-06-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.13 ([9e467ab](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/9e467abb94f189e85ca0bf59ea3cf8c5c50b4068))

## [5.0.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.12...v5.0.13) (2023-06-06)


### Bug Fixes

* 🐛 make admin group self manageable ([d915b34](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/d915b349482c3e99b29b669f7419dd8f69287d50))

## [5.0.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.11...v5.0.12) (2023-05-30)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.12 ([c6637f3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/c6637f3cf70f17258fecddf60024718d131006b8))

## [5.0.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.10...v5.0.11) (2023-05-30)


### Bug Fixes

* **deps:** update dependency jquery to v3.7.0 ([b511027](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/b51102735b8eade4ac8407225a154725795da1b6))

## [5.0.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.9...v5.0.10) (2023-05-11)


### Bug Fixes

* 🐛 Use correctly configured sub when doing user lookup ([f197d7b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/f197d7b31a5e210e02b2cd720cc96a2cdc8d1579))

## [5.0.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.8...v5.0.9) (2023-05-02)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.16.1 ([6eeb823](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/6eeb823bd6bb4da7dd80bdcae98cb776294ec13b))

## [5.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.7...v5.0.8) (2023-04-30)


### Bug Fixes

* **deps:** update dependency rxjs to v7.8.1 ([73f0c45](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/73f0c4564fe1267d17634dfae9b390805fc4b194))

## [5.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.6...v5.0.7) (2023-04-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.11 ([0884c85](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/0884c8561809e03f95d1c7aa2cb3b5408dd027c3))

## [5.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.5...v5.0.6) (2023-04-11)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.10 ([9b695c9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/9b695c91b9315521e6922e50e7d58ef2de2b0e70))

## [5.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.4...v5.0.5) (2023-04-11)


### Bug Fixes

* 🐛 Enable request rejection in approval process ([052bdd5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/052bdd50f3d171ebe2da44ffb4f6c233b785261a))

## [5.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.3...v5.0.4) (2023-04-11)


### Bug Fixes

* 🐛 Fix production transfer request ([f619d30](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/f619d3036ec953ead2535fa741664e476a954709))

## [5.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.2...v5.0.3) (2023-03-17)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.8.8 ([7b54bb8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/7b54bb8bb60ebe1a3be6c4b89f4cf641229175c8))

## [5.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.1...v5.0.2) (2023-03-14)


### Bug Fixes

* 🐛 fix login loop and default angular base href ([b632418](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/b63241818c083448bdb146a4eaa80b4b9f086783))

## [5.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v5.0.0...v5.0.1) (2023-03-14)


### Bug Fixes

* 🐛 Fix path for logging file ([c821216](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/c8212169e4ccafaaa98ab954a8e77d8811dd5819))

# [5.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.8...v5.0.0) (2023-03-14)


### Code Refactoring

* 💡 Deploy as war ([bf26450](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/bf26450e8cf31a704d28e18fe1cf749cae8fb09f))


### BREAKING CHANGES

* deployement method changed

## [4.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.7...v4.0.8) (2023-03-13)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.8.7 ([02fc18e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/02fc18e4960a521feb17b3938ee0c4ffbf795be8))

## [4.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.6...v4.0.7) (2023-03-11)


### Bug Fixes

* **deps:** update dependency jquery to v3.6.4 ([0fb1ad7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/0fb1ad7ea7b7a7cc0da42b1f8d3e1b5eb337e6dd))

## [4.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.5...v4.0.6) (2023-02-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.9 ([a7b5abf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/a7b5abfaeffb41e3fbe3ba787bbb9e8b428cfd34))

## [4.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.4...v4.0.5) (2023-02-21)


### Bug Fixes

* 🐛 Fix displaying Spring login page ([866a60a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/866a60aff324d20f3b8db3510f985e774c52db62))

## [4.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.3...v4.0.4) (2023-02-21)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.15.4 ([37fe33b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/37fe33bca78c24d613d795b67cf6c95668ba90d0))

## [4.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.2...v4.0.3) (2023-02-20)


### Bug Fixes

* 🐛 Fix HTML formatting in String and Select inputs ([15bb5ab](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/15bb5abacb64051fc36704882b16573c9a77037e))

## [4.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.1...v4.0.2) (2023-02-11)


### Bug Fixes

* 🐛 Fix setting updatedAt timestamp ([8581df8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/8581df83211c87aa28abacb5bd837ff15a141379))

## [4.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v4.0.0...v4.0.1) (2023-01-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.8 ([c322bfd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/c322bfdfdbbddcbe421f1522e069586e16d6145f))

# [4.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.40...v4.0.0) (2023-01-25)


### Bug Fixes

* **deps:** update dependency rxjs to v7.8.0 ([4f91db0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/4f91db0093856153978124f05ce2e309d9ce6133))


### Features

* 🎸 Store updated_at timestamp ([4456fc5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/4456fc5c2fe0b3c080f5caacb55f0ac1933bacb5))


### BREAKING CHANGES

* 🧨 Need to supply new option in attr_names

## [3.0.40](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.39...v3.0.40) (2023-01-04)


### Bug Fixes

* **deps:** update dependency @fortawesome/angular-fontawesome to v0.12.0 ([0ce1cd2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/0ce1cd29c7d3f856e0fb31f409692dd7426fb882))

## [3.0.39](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.38...v3.0.39) (2023-01-03)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.7 ([4052ee7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/4052ee77943953aa4ddba24e5c6bf03ffa17e56d))

## [3.0.38](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.37...v3.0.38) (2023-01-03)


### Bug Fixes

* **deps:** update dependency jquery to v3.6.3 ([bc09eb3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/bc09eb3636a8c0a5e579d4ca860f67c02e301538))

## [3.0.37](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.36...v3.0.37) (2023-01-03)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v15 ([982f3e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/982f3e77c83abe60ab4898b55f7e60fa5437c317))

## [3.0.36](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.35...v3.0.36) (2023-01-03)


### Bug Fixes

* **deps:** update angular monorepo to v15 ([6739962](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/6739962826f4e2fb4678096123ab7035cd474508))

## [3.0.35](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.34...v3.0.35) (2022-11-19)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.11 ([4b4754c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/4b4754cb040d92c7d26db6dd9805a619b4183a72))

## [3.0.34](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.33...v3.0.34) (2022-11-18)


### Bug Fixes

* **deps:** update font awesome to v6.2.1 ([81dd615](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/81dd615dd40405afc088336ffb173006ee02ee92))

## [3.0.33](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.32...v3.0.33) (2022-11-16)


### Bug Fixes

* **deps:** update nrwl monorepo to v15 ([3b3b134](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/3b3b1346d9ca35a9de6fdfb58164c79911b5a4a8))

## [3.0.32](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.31...v3.0.32) (2022-11-13)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.7 ([1f2e8ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/1f2e8ad765fef30f16d188d6488744bd745769e4))

## [3.0.31](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.30...v3.0.31) (2022-11-12)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.10 ([c35c60f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/c35c60fdfc80f8dd5a061f58b80fc1ab9c4a6dbb))

## [3.0.30](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.29...v3.0.30) (2022-11-07)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.9 ([47f16e8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/47f16e8521092cc3f0d60f427120ad08346b2612))

## [3.0.29](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.28...v3.0.29) (2022-10-29)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.6 ([d8ff88d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/d8ff88d615b8175f95b9009cd367b8b074babd84))

## [3.0.28](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.27...v3.0.28) (2022-10-29)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.8 ([037159a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/037159a78a029aafb6506842f61c498df0c0b453))

## [3.0.27](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.26...v3.0.27) (2022-10-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.5 ([e5f12e5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/e5f12e5efbfba3f3b7d1e4b24bb79a14c9dce536))

## [3.0.26](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.25...v3.0.26) (2022-10-23)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.7 ([f2fc62e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/f2fc62e6f7d28eb7fcc9315a586d95abd7d94c0f))

## [3.0.25](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.24...v3.0.25) (2022-10-15)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.6 ([5553159](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/5553159796d0cd46f1bd083aa4fe15d65380337b))
* **deps:** update angularmaterial monorepo to v14.2.5 ([7265685](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/72656850c7d01ee322d980509584ffcf58bea8ca))

## [3.0.24](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.23...v3.0.24) (2022-10-13)


### Bug Fixes

* 🐛 Fix session settings ([6f1a6eb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/6f1a6eb9aa86130f1842ad4c99e954023c53042b))

## [3.0.23](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.22...v3.0.23) (2022-10-09)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.5 ([5637fb4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/5637fb41f60754bfc14454ffb6cf4d6371ecc644))

## [3.0.22](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.21...v3.0.22) (2022-10-09)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.4 ([81cce82](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/81cce82a500adc0a2e0cd1183d5914f1a9125cbe))

## [3.0.21](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.20...v3.0.21) (2022-10-06)


### Bug Fixes

* **deps:** update dependency core-js to v3.25.5 ([9a8bd2f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/9a8bd2f15fb9918a7f1a5e0f6738cc18dbe1dd6e))

## [3.0.20](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.19...v3.0.20) (2022-10-05)


### Bug Fixes

* **deps:** update dependency rxjs to v7.5.7 ([86b8468](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/86b8468c04f49dee8f0e5c3a7b324e1619eac260))

## [3.0.19](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.18...v3.0.19) (2022-10-04)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([5c3a899](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/5c3a899c6297138f7d51a806df0b43ed1a86f6fe))

## [3.0.18](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.17...v3.0.18) (2022-10-04)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.3 ([cccbd0f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/cccbd0fb41d1e4f7aa4c2c9e667226a3ea999da1))

## [3.0.17](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.16...v3.0.17) (2022-10-04)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.4 ([9e55ce1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/9e55ce12a18e4375d11356ff63de72f2cdc25951))
* **deps:** update nrwl monorepo to v14.7.18 ([7429649](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/74296495cc7d248b280d0bd6b5e9d45221cca690))

## [3.0.16](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/compare/v3.0.15...v3.0.16) (2022-09-28)


### Bug Fixes

* first release in GitLab ([e6d41c9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/perun-spRegistration-app/commit/e6d41c9268f05aad19bdc7d1394459d386cde700))

## [3.0.15](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.14...v3.0.15) (2022-09-16)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.7.5 ([7e2dab0](https://github.com/CESNET/perun-spRegistration-app/commit/7e2dab02bb4e484ffed5a8c8b1adcbd9590903a4))

## [3.0.14](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.13...v3.0.14) (2022-09-13)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.1 ([cd54514](https://github.com/CESNET/perun-spRegistration-app/commit/cd54514d5765f938c6f41d93571048922c3258e1))

## [3.0.13](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.12...v3.0.13) (2022-09-13)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.1 ([268e617](https://github.com/CESNET/perun-spRegistration-app/commit/268e6175dae11c1b852188f9ef3ce6608da1a7f7))
* **deps:** update dependency core-js to v3.25.1 ([28efb01](https://github.com/CESNET/perun-spRegistration-app/commit/28efb01b4e39c81f106aa7292e0bac8d229b987f))

## [3.0.12](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.11...v3.0.12) (2022-09-07)


### Bug Fixes

* 🐛 fix reload when approval happens ([8e9b6da](https://github.com/CESNET/perun-spRegistration-app/commit/8e9b6da37d5685c4c3de6d672d7c03db4f16d60a))

## [3.0.11](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.10...v3.0.11) (2022-09-07)


### Bug Fixes

* 🐛 Fix mail templates ([336ece2](https://github.com/CESNET/perun-spRegistration-app/commit/336ece2f94a4e0f6f250c3134b2397bf1d66df63))

## [3.0.10](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.9...v3.0.10) (2022-09-07)


### Bug Fixes

* 🐛 Fix toolbox outputs ([ee31c3b](https://github.com/CESNET/perun-spRegistration-app/commit/ee31c3bcbc6bfc7a9da61f7363d1ea6f26aac477))

## [3.0.9](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.8...v3.0.9) (2022-09-06)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.6.4 ([9efd12f](https://github.com/CESNET/perun-spRegistration-app/commit/9efd12f3c6d11233e91ef7173e3b4b47a1d45436))

## [3.0.8](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.7...v3.0.8) (2022-09-05)


### Bug Fixes

* 🐛 Fix deleting managers group ([cb2d816](https://github.com/CESNET/perun-spRegistration-app/commit/cb2d8167cb898426d4f867020dfd276f9b28dff2))
* **deps:** update font awesome to v6.2.0 ([49193b9](https://github.com/CESNET/perun-spRegistration-app/commit/49193b91abcc0abf67952a14f19016a6d8c3f8d1))

## [3.0.7](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.6...v3.0.7) (2022-08-30)


### Bug Fixes

* **deps:** update dependency jquery to v3.6.1 ([b39a910](https://github.com/CESNET/perun-spRegistration-app/commit/b39a910827dd741260aabdb95994727e6a639cd7))

## [3.0.6](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.5...v3.0.6) (2022-08-29)


### Bug Fixes

* **deps:** update angular monorepo to v14.2.0 ([6da79ef](https://github.com/CESNET/perun-spRegistration-app/commit/6da79ef6c516e0859c732a688c7562cd3ccd2128))

## [3.0.5](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.4...v3.0.5) (2022-08-29)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.2.0 ([5ca9ab0](https://github.com/CESNET/perun-spRegistration-app/commit/5ca9ab02c595b1fa9237c1f6ced306782b63aeef))
* **deps:** update dependency core-js to v3.25.0 ([43eaabe](https://github.com/CESNET/perun-spRegistration-app/commit/43eaabe15e1588d54d98fa3a61a26d2812e48079))
* **deps:** update dependency org.jsoup:jsoup to v1.15.3 ([3c9bb79](https://github.com/CESNET/perun-spRegistration-app/commit/3c9bb79b4b9a6d924f930a8dd9ff3d54c0d54c53))
* **deps:** update nrwl monorepo to v14.5.10 ([3d12a5a](https://github.com/CESNET/perun-spRegistration-app/commit/3d12a5ad84ec060e38c95696c2ba7f791c41ca5c))

## [3.0.4](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.3...v3.0.4) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.3 ([5acb0d7](https://github.com/CESNET/perun-spRegistration-app/commit/5acb0d768a7f809c7efa4288b0a86aacb889ac89))

## [3.0.3](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.2...v3.0.3) (2022-08-19)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.5.7 ([fada75b](https://github.com/CESNET/perun-spRegistration-app/commit/fada75b6e205af90fa69a4c9e76c258b1ff0d8bd))

## [3.0.2](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.1...v3.0.2) (2022-08-19)


### Bug Fixes

* **deps:** update angular monorepo ([286738b](https://github.com/CESNET/perun-spRegistration-app/commit/286738b645eb1c4c74df36d755aa5e7599a53bcb))

## [3.0.1](https://github.com/CESNET/perun-spRegistration-app/compare/v3.0.0...v3.0.1) (2022-08-15)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.1.2 ([e084b70](https://github.com/CESNET/perun-spRegistration-app/commit/e084b70f3b915c9b5b973dfc3fa3e5ce3fe892f2))

# [3.0.0](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.12...v3.0.0) (2022-08-08)


### Bug Fixes

* 🐛 Remove nondisplayed fields from facility edit ([70e5154](https://github.com/CESNET/perun-spRegistration-app/commit/70e5154a32a81b7e8e2f193c07f2f52ee49caee3))
* **deps:** update angularmaterial monorepo to v14.1.1 ([7aeaad9](https://github.com/CESNET/perun-spRegistration-app/commit/7aeaad9b0cf7b0ada61ca15b8ed79389ed680845))
* **deps:** update nrwl monorepo to v14.5.4 ([5fc900c](https://github.com/CESNET/perun-spRegistration-app/commit/5fc900ca75afa3a3ee1fdf51cd5202cff91960d7))


### Features

* Different required inputs for test and prod ([fa3ab43](https://github.com/CESNET/perun-spRegistration-app/commit/fa3ab43e7e70b9df8910803b6d4291f8952b486a))


### BREAKING CHANGES

* different property for "required" for inputs

## [2.1.12](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.11...v2.1.12) (2022-08-08)


### Bug Fixes

* **deps:** update angular monorepo to v14.1.1 ([402d549](https://github.com/CESNET/perun-spRegistration-app/commit/402d54911cb1c06eb604c606714a8ee454d7a171))

## [2.1.11](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.10...v2.1.11) (2022-08-03)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.5.2 ([87056f7](https://github.com/CESNET/perun-spRegistration-app/commit/87056f7b33b660a1ecd2c9ae0ad85005b8baf0dc))

## [2.1.10](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.9...v2.1.10) (2022-08-03)


### Bug Fixes

* **deps:** update dependency core-js to v3.24.1 ([9edc993](https://github.com/CESNET/perun-spRegistration-app/commit/9edc993b25cb2f228c4528020134d577944284cd))
* **deps:** update font awesome to v6.1.2 ([c3a9d38](https://github.com/CESNET/perun-spRegistration-app/commit/c3a9d38852a44764e70df12803eac78349d04255))

## [2.1.9](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.8...v2.1.9) (2022-07-25)


### Bug Fixes

* **deps:** update angular monorepo ([591dfe9](https://github.com/CESNET/perun-spRegistration-app/commit/591dfe97fe2c5e8f5e6acc2195cffd826ad00ff5))

## [2.1.8](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.7...v2.1.8) (2022-07-25)


### Bug Fixes

* **deps:** update dependency bootstrap to v4.6.2 ([4ade814](https://github.com/CESNET/perun-spRegistration-app/commit/4ade8141bea3eebe8f02309a1994998c46ee13de))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.2 ([e54d72e](https://github.com/CESNET/perun-spRegistration-app/commit/e54d72e7b3201aa6cad1dba3ed255a9f9994fae3))

## [2.1.7](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.6...v2.1.7) (2022-07-22)


### Bug Fixes

* **deps:** update dependency core-js to v3.23.5 ([339c6ba](https://github.com/CESNET/perun-spRegistration-app/commit/339c6ba01f39b1d075e627afe03ee3ce5f9e981d))

## [2.1.6](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.5...v2.1.6) (2022-07-20)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.1.0 ([367444f](https://github.com/CESNET/perun-spRegistration-app/commit/367444f83adbc7c203ae8735b556b0014cb23ec4))
* **deps:** update nrwl monorepo to v14.4.3 ([864a8c3](https://github.com/CESNET/perun-spRegistration-app/commit/864a8c30668ccf593c5768aa0fc70fdfced01491))

## [2.1.5](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.4...v2.1.5) (2022-07-20)


### Bug Fixes

* **deps:** update angular monorepo to v14.0.6 ([a312506](https://github.com/CESNET/perun-spRegistration-app/commit/a312506eaf6a64a2b64044b0d0bc25e5d9371d45))
* **deps:** update dependency rxjs to v7.5.6 ([4c4f5d4](https://github.com/CESNET/perun-spRegistration-app/commit/4c4f5d4a527f44058c3e0b45248cc1f68c4196d4))

## [2.1.4](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.3...v2.1.4) (2022-07-13)


### Bug Fixes

* **deps:** update dependency core-js to v3.23.4 ([f3a13b9](https://github.com/CESNET/perun-spRegistration-app/commit/f3a13b9642406657bf0ee675fc7128b66ce71525))

## [2.1.3](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.2...v2.1.3) (2022-07-12)


### Bug Fixes

* 🐛 Fix CSRF for API requests ([2eb81fd](https://github.com/CESNET/perun-spRegistration-app/commit/2eb81fde1799717b2266bb1a31bb4c7b243c33b4))

## [2.1.2](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.1...v2.1.2) (2022-07-12)


### Bug Fixes

* 🐛 Fix security for assets ([f182f9d](https://github.com/CESNET/perun-spRegistration-app/commit/f182f9d1bb39314a19397d4d70b1da3bd3f24800))

## [2.1.1](https://github.com/CESNET/perun-spRegistration-app/compare/v2.1.0...v2.1.1) (2022-07-12)


### Bug Fixes

* 🐛 Fix admins setup in facility admins ([95a2190](https://github.com/CESNET/perun-spRegistration-app/commit/95a2190786a88db3a856eae0d089ee0837dfb0b7))

# [2.1.0](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.12...v2.1.0) (2022-07-12)


### Bug Fixes

* **deps:** update angular monorepo to v14.0.5 ([b68e291](https://github.com/CESNET/perun-spRegistration-app/commit/b68e291d2a673fcb5ef25f360c90bc585592fbc5))


### Features

* 🎸 OIDC Authentication ([abb6a8c](https://github.com/CESNET/perun-spRegistration-app/commit/abb6a8cf58636611a3231aeedbb1608212198596))

## [2.0.12](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.11...v2.0.12) (2022-07-09)


### Bug Fixes

* typo/Slovaczech in Remove service translation ([5c3c6cd](https://github.com/CESNET/perun-spRegistration-app/commit/5c3c6cdc38d1015fc38b81ccd9fc0f8298a3e41f))

## [2.0.11](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.10...v2.0.11) (2022-07-08)


### Bug Fixes

* **deps:** update nrwl monorepo to v14.4.2 ([aad655f](https://github.com/CESNET/perun-spRegistration-app/commit/aad655fd989760a30ea2b9e7058dcd5fdac8430e))

## [2.0.10](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.9...v2.0.10) (2022-07-08)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.15.2 ([c006f8f](https://github.com/CESNET/perun-spRegistration-app/commit/c006f8fc7080aed2283da4ec5eda104d1cd93b14))

## [2.0.9](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.8...v2.0.9) (2022-07-04)


### Bug Fixes

* 🐛 displaying new service form (preselection of protocol) ([0e3e46e](https://github.com/CESNET/perun-spRegistration-app/commit/0e3e46e4d346d4a630ff5631113e76e25d490818))
* 🐛 Fix formatting in lint ([2be3404](https://github.com/CESNET/perun-spRegistration-app/commit/2be3404a33cf2934fe6386d1788114d6fffcd5c8))

## [2.0.8](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.7...v2.0.8) (2022-07-04)


### Bug Fixes

* **deps:** update angular monorepo to v14.0.4 ([b943e81](https://github.com/CESNET/perun-spRegistration-app/commit/b943e81800287ac477f24063cb8d889f938d604f))

## [2.0.7](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.6...v2.0.7) (2022-07-04)


### Bug Fixes

* **deps:** update dependency core-js to v3.23.3 ([6ed19e9](https://github.com/CESNET/perun-spRegistration-app/commit/6ed19e9edb708587ab3e3e497bb060a42d2ecc60))

## [2.0.6](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.5...v2.0.6) (2022-07-04)


### Bug Fixes

* 🐛 Fix building (dependency @nrwl/eslint-plugin-nx) ([5d59d1b](https://github.com/CESNET/perun-spRegistration-app/commit/5d59d1b1207f5362d57d4d18acdfc671dfb6e2e9))
* **deps:** update angularmaterial monorepo to v14.0.4 ([f2f37b9](https://github.com/CESNET/perun-spRegistration-app/commit/f2f37b9faf08f6fa93a40a3b09fdf350ca5f7bb9))

## [2.0.5](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.4...v2.0.5) (2022-06-27)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.0.3 ([e6d28dd](https://github.com/CESNET/perun-spRegistration-app/commit/e6d28dd66fe4be8b520a23e038c8fbb972dca317))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.1 ([a2d651f](https://github.com/CESNET/perun-spRegistration-app/commit/a2d651fc235bfcabcd2a925b352dc95ac1127c35))

## [2.0.4](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.3...v2.0.4) (2022-06-27)


### Bug Fixes

* **deps:** update angular monorepo to v14.0.3 ([16bf186](https://github.com/CESNET/perun-spRegistration-app/commit/16bf1866c44156e1c49ef1d316c53993bf648194))

## [2.0.3](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.2...v2.0.3) (2022-06-24)


### Bug Fixes

* **deps:** update dependency core-js to v3.23.2 ([e238c97](https://github.com/CESNET/perun-spRegistration-app/commit/e238c979180ea49d75f3bab4dd68c35696ac872a))

## [2.0.2](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.1...v2.0.2) (2022-06-20)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v14.0.2 ([8866650](https://github.com/CESNET/perun-spRegistration-app/commit/88666504be6f79491cbb0c60b14acb500538cbdb))
* **deps:** update dependency @fortawesome/angular-fontawesome to v0.11.1 ([ef754d0](https://github.com/CESNET/perun-spRegistration-app/commit/ef754d089ef2b32b3d34faf3a55509486b3d0986))

## [2.0.1](https://github.com/CESNET/perun-spRegistration-app/compare/v2.0.0...v2.0.1) (2022-06-15)


### Bug Fixes

* **deps:** update dependency @fortawesome/angular-fontawesome to v0.11.0 ([c5160e9](https://github.com/CESNET/perun-spRegistration-app/commit/c5160e97982e06e38e6d810e7c4f169430e7a2ff))
* **deps:** update dependency core-js to v3.23.1 ([422aa70](https://github.com/CESNET/perun-spRegistration-app/commit/422aa704791be34ae335755f92163120ba220f6e))

# [2.0.0](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.36...v2.0.0) (2022-06-15)


### Features

* 🎸 Update to angular 14 ([c95d6ea](https://github.com/CESNET/perun-spRegistration-app/commit/c95d6ea05eded5ce8fec11a06324e1131482ecfe))


### BREAKING CHANGES

* 🧨 Angular 14

## [1.3.36](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.35...v1.3.36) (2022-06-15)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.11 ([c8172c5](https://github.com/CESNET/perun-spRegistration-app/commit/c8172c5fe71cc6783d783a5ecbf0015e350a3a55))

## [1.3.35](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.34...v1.3.35) (2022-06-15)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.9 ([06dd63f](https://github.com/CESNET/perun-spRegistration-app/commit/06dd63f56f838c783ae0e27a7572baa4f448281a))

## [1.3.34](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.33...v1.3.34) (2022-05-30)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.10 ([7052243](https://github.com/CESNET/perun-spRegistration-app/commit/705224343eaab9e065e8fd11dda41b9c20f6bbb4))

## [1.3.33](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.32...v1.3.33) (2022-05-30)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.8 ([308ecfc](https://github.com/CESNET/perun-spRegistration-app/commit/308ecfc86cb2b09519e8e81f0d0742da7abb5dd1))
* **deps:** update dependency core-js to v3.22.7 ([5817775](https://github.com/CESNET/perun-spRegistration-app/commit/5817775f6fb80bc554566302cd7c8f4bd1d6ce45))
* **deps:** update nrwl monorepo to v14.1.9 ([bd2aab5](https://github.com/CESNET/perun-spRegistration-app/commit/bd2aab5695a69fef6461f9d9d49df3ddd33874aa))

## [1.3.32](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.31...v1.3.32) (2022-05-23)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.9 ([af9f785](https://github.com/CESNET/perun-spRegistration-app/commit/af9f78572d53a33b49cf7e4e0a4fed791258d2df))
* **deps:** update nrwl monorepo to v14 ([7b00eb0](https://github.com/CESNET/perun-spRegistration-app/commit/7b00eb03e322c8abef514765ece5593ee1256903))

## [1.3.31](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.30...v1.3.31) (2022-05-23)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.7 ([9d6a269](https://github.com/CESNET/perun-spRegistration-app/commit/9d6a2699e52af23e8f8a9eea484901b157b1fe4d))
* **deps:** update dependency core-js to v3.22.6 ([a0edbe7](https://github.com/CESNET/perun-spRegistration-app/commit/a0edbe7f9eb6e0801376883488c4d4fbba9f1541))
* **deps:** update dependency org.jsoup:jsoup to v1.15.1 ([b32803e](https://github.com/CESNET/perun-spRegistration-app/commit/b32803e69956499c4c8fdc634978caed522e6613))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.0 ([390cc77](https://github.com/CESNET/perun-spRegistration-app/commit/390cc77098304cab41b6bb0c5f2052d5a694dcec))
* **deps:** update nrwl monorepo ([f622c07](https://github.com/CESNET/perun-spRegistration-app/commit/f622c07011d449e06d0e222f6fad731b5fcfc0a2))

## [1.3.30](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.29...v1.3.30) (2022-05-09)


### Bug Fixes

* **deps:** update gui deps ([bdda10b](https://github.com/CESNET/perun-spRegistration-app/commit/bdda10b83d303e93fe4d65aa45a39a12ac22633c))

## [1.3.29](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.28...v1.3.29) (2022-04-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.7 ([7e46cce](https://github.com/CESNET/perun-spRegistration-app/commit/7e46cceb97889b5f01a13028babb1e05f44f51b9))

## [1.3.28](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.27...v1.3.28) (2022-04-19)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.3 ([deb17d1](https://github.com/CESNET/perun-spRegistration-app/commit/deb17d1c285de1d7bd13110044e9e5ae88310d07))

## [1.3.27](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.26...v1.3.27) (2022-04-19)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.3 ([0d9bafd](https://github.com/CESNET/perun-spRegistration-app/commit/0d9bafd64ac2242a156ab92371ad84a1f661acfa))

## [1.3.26](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.25...v1.3.26) (2022-04-12)


### Bug Fixes

* **deps:** update dependency eslint to v8.13.0 ([053e423](https://github.com/CESNET/perun-spRegistration-app/commit/053e4232f348bba500ae236d0e82b834ffc12f18))

## [1.3.25](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.24...v1.3.25) (2022-04-04)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.1 ([c80328a](https://github.com/CESNET/perun-spRegistration-app/commit/c80328a2f6582db4888918e2f4a4a7c83237c832))

## [1.3.24](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.23...v1.3.24) (2022-04-01)


### Bug Fixes

* 🐛 Update spring to v2.6.6 ([8743f4c](https://github.com/CESNET/perun-spRegistration-app/commit/8743f4c4e54dac38f8a50dcdf0a8441f5d4eda56))

## [1.3.23](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.22...v1.3.23) (2022-03-31)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.2 ([5280338](https://github.com/CESNET/perun-spRegistration-app/commit/5280338c68f6d63fbed58306335086a59c0e9914))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.5 ([72b2770](https://github.com/CESNET/perun-spRegistration-app/commit/72b2770f54fc233ca2ab8153dd1dea08df9c55bd))

## [1.3.22](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.21...v1.3.22) (2022-03-29)


### Bug Fixes

* **deps:** update dependency eslint to v8.12.0 ([dacee34](https://github.com/CESNET/perun-spRegistration-app/commit/dacee34a1d82e9e3a8ebcb2ec4dd80c4f8c0f7dd))

## [1.3.21](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.20...v1.3.21) (2022-03-23)


### Bug Fixes

* **deps:** update dependency @fortawesome/fontawesome-svg-core to v6 ([12c8754](https://github.com/CESNET/perun-spRegistration-app/commit/12c8754574cc08fced6fe8dba53f088c2075856a))

## [1.3.20](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.19...v1.3.20) (2022-03-23)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.3.0 ([83e0620](https://github.com/CESNET/perun-spRegistration-app/commit/83e062048952142a35d68e74a9dcdd0003d3a294))
* **deps:** update font awesome ([6e661bb](https://github.com/CESNET/perun-spRegistration-app/commit/6e661bbf092e8dc320922aaf92475e1bb55dbd78))

## [1.3.19](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.18...v1.3.19) (2022-03-23)


### Bug Fixes

* **deps:** update angular monorepo to v13.3.0 ([52d4540](https://github.com/CESNET/perun-spRegistration-app/commit/52d4540626fb0233b94f8750f1955b42f2969d3d))
* **deps:** update dependency rxjs to v7.5.5 ([e05cd76](https://github.com/CESNET/perun-spRegistration-app/commit/e05cd764d89f1618b17aa7f0704096bede30c2ef))

## [1.3.18](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.17...v1.3.18) (2022-03-15)


### Bug Fixes

* **deps:** update dependency eslint to v8.11.0 ([9c301c3](https://github.com/CESNET/perun-spRegistration-app/commit/9c301c3155d3b491ca8a23c09cc465260413dfb2))

## [1.3.17](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.16...v1.3.17) (2022-03-09)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.2.5 ([10339fc](https://github.com/CESNET/perun-spRegistration-app/commit/10339fc14746eb34ba372067005e195bf4936b2b))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.4 ([8fdbf80](https://github.com/CESNET/perun-spRegistration-app/commit/8fdbf80ee44e1206f1e1ac46b49ab76c2b416335))

## [1.3.16](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.15...v1.3.16) (2022-03-09)


### Bug Fixes

* **deps:** update angular monorepo ([f97d2d5](https://github.com/CESNET/perun-spRegistration-app/commit/f97d2d53f54ec824181e5e3ca46810225e36b454))

## [1.3.15](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.14...v1.3.15) (2022-03-01)


### Bug Fixes

* **deps:** update dependency eslint to v8.10.0 ([d008e7c](https://github.com/CESNET/perun-spRegistration-app/commit/d008e7cc8cb877a4975f0cf92ab03e4b09074162))

## [1.3.14](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.13...v1.3.14) (2022-02-21)


### Bug Fixes

* **deps:** update dependency rxjs to v7.5.4 ([cf7c957](https://github.com/CESNET/perun-spRegistration-app/commit/cf7c9573072be0d19cb56db373c1eef90ab5a71f))

## [1.3.13](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.12...v1.3.13) (2022-02-21)


### Bug Fixes

* **deps:** update angular monorepo to v13.2.3 ([96edcf7](https://github.com/CESNET/perun-spRegistration-app/commit/96edcf7060c6f39ea09c98b74b87632392487542))

## [1.3.12](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.11...v1.3.12) (2022-02-17)


### Bug Fixes

* **deps:** update dependency eslint to v8.9.0 ([c35a9eb](https://github.com/CESNET/perun-spRegistration-app/commit/c35a9eb0c047564ae996b9c2af56b2642036290a))

## [1.3.11](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.10...v1.3.11) (2022-02-17)


### Bug Fixes

* **deps:** update dependency @fortawesome/free-solid-svg-icons to v6 ([da6a88e](https://github.com/CESNET/perun-spRegistration-app/commit/da6a88eeee3d334a498b79949cf506cccf8b2b3c))
* **deps:** update dependency core-js to v3.21.1 ([7b61555](https://github.com/CESNET/perun-spRegistration-app/commit/7b61555c88050082c4a72ea26ee5ccb5d2b33375))

## [1.3.10](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.9...v1.3.10) (2022-02-17)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.2.3 ([7ce6b7e](https://github.com/CESNET/perun-spRegistration-app/commit/7ce6b7ec0889733b1ae9bbe60b250d82b43a41c4))
* **deps:** update dependency @fortawesome/fontawesome-svg-core to v1.3.0 ([cecc975](https://github.com/CESNET/perun-spRegistration-app/commit/cecc9759a2b025a22957caaf71c629244609e166))

## [1.3.9](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.8...v1.3.9) (2022-02-17)


### Bug Fixes

* **deps:** update angular monorepo to v13.2.2 ([9652aa3](https://github.com/CESNET/perun-spRegistration-app/commit/9652aa3db621a8cb42452ac814ea23b7c4df6954))

## [1.3.8](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.7...v1.3.8) (2022-02-01)


### Bug Fixes

* **deps:** update angular monorepo to v13.2.0 ([0aa4896](https://github.com/CESNET/perun-spRegistration-app/commit/0aa4896abbf280f71c4e1e09fdbc23af018ac020))

## [1.3.7](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.6...v1.3.7) (2022-02-01)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.2.0 ([b393e83](https://github.com/CESNET/perun-spRegistration-app/commit/b393e837c1d824c5ba76d82a1f7daf974a3753d5))

## [1.3.6](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.5...v1.3.6) (2022-02-01)


### Bug Fixes

* **deps:** update dependency eslint to v8.8.0 ([830d9a1](https://github.com/CESNET/perun-spRegistration-app/commit/830d9a1c9ea5b7bc0cce0c89bf003437b341e506))

## [1.3.5](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.4...v1.3.5) (2022-01-24)


### Bug Fixes

* **deps:** update angularmaterial monorepo to v13.1.3 ([2717ba6](https://github.com/CESNET/perun-spRegistration-app/commit/2717ba637bf7361893930d668883a802418c8a3f))

## [1.3.4](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.3...v1.3.4) (2022-01-24)


### Bug Fixes

* **deps:** update angular monorepo to v13.1.3 ([970e55d](https://github.com/CESNET/perun-spRegistration-app/commit/970e55df9af881d0e5cd7fb19ff286a7657ef1e8))

## [1.3.3](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.2...v1.3.3) (2022-01-19)


### Bug Fixes

* **deps:** update dependency @ngx-translate/core to v14 ([e50be50](https://github.com/CESNET/perun-spRegistration-app/commit/e50be50902a3824c35926a49a02e0a8151c8a9da))

## [1.3.2](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.1...v1.3.2) (2022-01-19)


### Bug Fixes

* **deps:** update dependency eslint to v8 ([c9b0e97](https://github.com/CESNET/perun-spRegistration-app/commit/c9b0e97f81097fbae119159572a251ff5ec5c7a5))

## [1.3.1](https://github.com/CESNET/perun-spRegistration-app/compare/v1.3.0...v1.3.1) (2022-01-19)


### Bug Fixes

* **deps:** update dependency core-js to v3.20.3 ([f60442e](https://github.com/CESNET/perun-spRegistration-app/commit/f60442e57c5c58de702c9b17baf2cb5de302a945))
* **deps:** update dependency rxjs to v7.5.2 ([2698370](https://github.com/CESNET/perun-spRegistration-app/commit/26983702769e2351a01f87e15702c62cc18c84d1))

# [1.3.0](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.6...v1.3.0) (2022-01-19)


### Bug Fixes

* **deps:** update angular monorepo to v13.1.2 ([b1a706c](https://github.com/CESNET/perun-spRegistration-app/commit/b1a706c8e44891cbefcdb3a9560c80d9c05923b9))
* **deps:** update angularmaterial monorepo to v13.1.2 ([796a216](https://github.com/CESNET/perun-spRegistration-app/commit/796a21689498b4e17f138a9e18a19bdeae6aba14))
* **deps:** update dependency bootstrap to v4.6.1 ([137dfff](https://github.com/CESNET/perun-spRegistration-app/commit/137dfff0fbb647d00c712c05224ce10bc7ea11bd))


### Features

* 🎸 new angular version ([688896f](https://github.com/CESNET/perun-spRegistration-app/commit/688896f234bb5656d6f7bcc5d4d325728979da77))

## [1.2.6](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.5...v1.2.6) (2022-01-18)


### Bug Fixes

* **deps:** update dependency @fortawesome/angular-fontawesome to ^0.10.0 ([6210ef4](https://github.com/CESNET/perun-spRegistration-app/commit/6210ef46f9364a244e6332d008cacd592f451227))

## [1.2.5](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.4...v1.2.5) (2022-01-13)


### Bug Fixes

* Fixed dependencies ([6921e5c](https://github.com/CESNET/perun-spRegistration-app/commit/6921e5c51dae09c59bf9374197389ff324e1c773))

## [1.2.4](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.3...v1.2.4) (2022-01-11)


### Bug Fixes

* Added check that services are initialized before unsubscription ([64ea88d](https://github.com/CESNET/perun-spRegistration-app/commit/64ea88d2306b6600fa3aceb33b9ae84dc40dbee4))

## [1.2.3](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.2...v1.2.3) (2022-01-06)


### Bug Fixes

* fix session problems ([4b8538c](https://github.com/CESNET/perun-spRegistration-app/commit/4b8538c6787f916bbb42a78114650d77f3b49a93))

## [1.2.2](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.1...v1.2.2) (2022-01-06)


### Bug Fixes

* 🐛 Fix calling wrong method in "My services" ([3350dcb](https://github.com/CESNET/perun-spRegistration-app/commit/3350dcb0462ac729cc3d1504a36786c27242eb2c))

## [1.2.1](https://github.com/CESNET/perun-spRegistration-app/compare/v1.2.0...v1.2.1) (2021-12-10)


### Bug Fixes

* 🐛 Fix cesnet favicon ([5b4f575](https://github.com/CESNET/perun-spRegistration-app/commit/5b4f57549bc08dcd33cacfb865541ac42682835c))

# [1.2.0](https://github.com/CESNET/perun-spRegistration-app/compare/v1.1.0...v1.2.0) (2021-12-10)


### Features

* 🎸 Favicons from config ([8f5d3cc](https://github.com/CESNET/perun-spRegistration-app/commit/8f5d3cc3a46c32e3e7bb599923e745483a92cb6d))

# [1.1.0](https://github.com/CESNET/perun-spRegistration-app/compare/v1.0.3...v1.1.0) (2021-11-18)


### Features

* 🎸 Display also external services ([15841de](https://github.com/CESNET/perun-spRegistration-app/commit/15841de53785e5fd43cd20ca6018814740981edc))

## [1.0.3](https://github.com/CESNET/perun-spRegistration-app/compare/v1.0.2...v1.0.3) (2021-10-25)


### Bug Fixes

* 🐛 Fix dependencies in angular ([c469c5a](https://github.com/CESNET/perun-spRegistration-app/commit/c469c5a7695662dec286aa5109ada2ef169f1a16))

## [1.0.2](https://github.com/CESNET/perun-spRegistration-app/compare/v1.0.1...v1.0.2) (2021-10-21)


### Bug Fixes

* 🐛 fix routing declaration ([5df165e](https://github.com/CESNET/perun-spRegistration-app/commit/5df165e5e8967e1ec356d7cc5de4932c5f843dc9))
* 🐛 fix Submitting request possible null pointer exception ([bd61351](https://github.com/CESNET/perun-spRegistration-app/commit/bd61351e7fc6e733c430f96476ecdbaecb38a4a4))
* 🐛 Reveal form if submit has failed ([363ee60](https://github.com/CESNET/perun-spRegistration-app/commit/363ee60485b517a76695e844982931f60244caee))

## [1.0.1](https://github.com/CESNET/perun-spRegistration-app/compare/v1.0.0...v1.0.1) (2021-10-21)


### Bug Fixes

* 🐛 Fix displaying not-dispalyed items in request-edit ([83fb6e5](https://github.com/CESNET/perun-spRegistration-app/commit/83fb6e57d28a832ea86dcba051cef0488b163ebb))

## [v1.0.0](https://github.com/CESNET/perun-spRegistration-app/compare/v0.2.1...v1.0.0) (2021-08-17)

* Initial release for automated semantic release

## [v0.2.1](https://github.com/CESNET/perun-spRegistration-app/compare/v0.2.0...v0.2.1) (2021-04-02)

* Bunch of GUI fixes and improvements

## [v0.2.0](https://github.com/CESNET/perun-spRegistration-app/compare/v0.1.0...v0.2.0) (2021-03-31)

* Added mvn wrapper
* Bunch of fixes
* feature to remove admins
* GUI dependencies audit fix

## [v0.1.0](https://github.com/CESNET/perun-spRegistration-app/releases/tag/v0.1.0) (2020-12-08)

* Initial release
